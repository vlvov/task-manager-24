package ru.t1.vlvov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.vlvov.tm.api.repository.*;
import ru.t1.vlvov.tm.api.service.*;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.command.project.*;
import ru.t1.vlvov.tm.command.task.*;
import ru.t1.vlvov.tm.command.system.*;
import ru.t1.vlvov.tm.command.user.*;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.exception.AbstractException;
import ru.t1.vlvov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.vlvov.tm.exception.system.CommandNotSupportedException;
import ru.t1.vlvov.tm.repository.CommandRepository;
import ru.t1.vlvov.tm.repository.ProjectRepository;
import ru.t1.vlvov.tm.repository.TaskRepository;
import ru.t1.vlvov.tm.repository.UserRepository;
import ru.t1.vlvov.tm.service.*;
import ru.t1.vlvov.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Set;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final String PACKAGE_COMMANDS = "ru.t1.vlvov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, projectService, taskService);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void registry(@NotNull Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initDemoData() {
        projectService.create("First", "First Project").setStatus(Status.NOT_STARTED);
        projectService.create("Second", "Second Project").setStatus(Status.IN_PROGRESS);
        projectService.create("Third", "Third Project").setStatus(Status.NOT_STARTED);
        projectService.create("Force", "Force be with you").setStatus(Status.COMPLETED);

        taskService.create("Third", "Third Task");
        taskService.create("Second", "Second Task");
        taskService.create("First", "First Task");

        userService.create("USER1", "PASS1");
        userService.create("USER2", "PASS2");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(@NotNull final String[] args) {
        if (processArguments(args)) System.exit(0);
        initDemoData();
        innitLogger();
        processCommands();
    }

    private boolean processArguments(@Nullable final String args[]) {
        if (args == null || args.length == 0) return false;
        @Nullable final String argument = args[0];
        if (argument == null) return false;
        processArgument(argument);
        return true;
    }

    private void processArgument(@NotNull final String argument) {
        @Nullable final AbstractCommand command = commandService.getCommandByArgument(argument);
        if (command == null) throw new ArgumentNotSupportedException(argument);
        command.execute();
    }

    private void innitLogger() {
        loggerService.info("**WELCOME TO TASK-MANAGER**");
    }

    private void processCommands() {
        String command;
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                loggerService.command(command);
                processCommand(command);
                System.out.println("OK");
            } catch (AbstractException e) {
                loggerService.error(e);
                System.err.println("FAIL");
            }
        }
    }

    private void processCommand(final String name) {
        @Nullable final AbstractCommand command = commandService.getCommandByName(name);
        if (command == null) throw new CommandNotSupportedException(name);
        getAuthService().checkRoles(command.getRoles());
        command.execute();
    }

}
