package ru.t1.vlvov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.comparator.CreatedComparator;
import ru.t1.vlvov.tm.comparator.NameComparator;
import ru.t1.vlvov.tm.comparator.StatusComparator;

import java.util.Comparator;

@Getter
public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private Comparator comparator;

    Sort(@NotNull final String displayName, @NotNull final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

}
