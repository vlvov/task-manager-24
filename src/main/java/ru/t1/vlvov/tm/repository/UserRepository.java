package ru.t1.vlvov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.IUserRepository;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.util.HashUtil;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return add(user);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        return add(user);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @NotNull final Role role) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        return add(user);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return models.stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return models.stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst().orElse(null);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return models.stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return models.stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}
