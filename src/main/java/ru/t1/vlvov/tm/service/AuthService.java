package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.service.IAuthService;
import ru.t1.vlvov.tm.api.service.IUserService;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.exception.entity.UserNotFoundException;
import ru.t1.vlvov.tm.exception.field.LoginEmptyException;
import ru.t1.vlvov.tm.exception.field.PasswordEmptyException;
import ru.t1.vlvov.tm.exception.user.AccessDeniedException;
import ru.t1.vlvov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.vlvov.tm.exception.user.PermissionException;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Nullable
    @Override
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.isLocked()) throw new AccessDeniedException();
        @NotNull final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public void checkRoles(@Nullable Role[] roles) {
        if (roles == null) return;
        if (!Arrays.stream(roles).anyMatch(r -> r == getUser().getRole())) throw new PermissionException();
    }

}
