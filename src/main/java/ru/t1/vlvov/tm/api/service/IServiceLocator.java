package ru.t1.vlvov.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IServiceLocator {

    @Nullable
    ICommandService getCommandService();

    @Nullable
    ILoggerService getLoggerService();

    @Nullable
    IProjectService getProjectService();

    @Nullable
    IProjectTaskService getProjectTaskService();

    @Nullable
    ITaskService getTaskService();

    @Nullable
    IAuthService getAuthService();

    @Nullable
    IUserService getUserService();

}